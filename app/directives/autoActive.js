'use strict';

window.location.hash = '#!/'; //All hash paths need to start with a /, it happens automaticaly with ngResource and the like...

angular.module("myApp.autoActive", [])
.directive("autoActive",
        ['$location', function ($location) {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element) {

                function setActive() {
                	console.log("setActive");
                    var path = $location.path();
                    console.log(path);
                    if (path) {
                        angular.forEach(element.find('li'), function (li) {
                            var anchor = li.querySelector('a');
                            var compare = '#!' + path + '(?=\\?|$)';
                           // console.log("menu->", anchor.href);
                           // console.log("compare->", compare);
                           // console.log("its a match?", !!compare);
                            if (anchor.href.match(compare)) {
                                angular.element(li.querySelector('a')).addClass('active');
                            } else {
                                angular.element(li.querySelector('a')).removeClass('active');
                            }
                        });
                    }
                }

                setActive();

                scope.$on('$locationChangeSuccess', setActive);
            }
        }
    }]);
//the module we are demonstrating:
