'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', '$http', function($scope, $http) {
	$scope.message = 'Its alive!!!';
   	$scope.weather = {};
   	$scope.capitals = {};
   	$scope.hidden = true;
	$scope.imagesMap= {
	    "Partly Cloudy": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/partly-cloudy.png?",
        "Mostly Sunny": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/mostly-sunny.png?",
        "Sunny": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/sunny.png?",
        "Scattered Showers": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/scattered-showers.png?",
        "Mostly Cloudy": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/mostly-cloudy.png?",
        "Thunderstorms": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/thunderstorm.png?",
        "Scattered Thunderstorms": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/scattered-thunderstorms.png?",
        "Breezy": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/wind.png?",
        "Windy": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/wind.png?",
        "Cloudy": "https://weather.com/sites/all/modules/custom/angularmods/app/shared/wxicon/png/cloudy.png?",
    };
   
    var asyncCallWeather = function(url) {
    	$scope.message = url;
    	$http({
	    	url : url,
	    	method : "GET"
	    }).success(function(data, status, headers, config){
	    	$scope.weather.valid = true;
	    	$scope.weather.response = data.query.results.channel;	
	    }).error(function(data, status, headers, config){
	    	$scope.weather.valid = false;
	    });
    };

    var asyncCallCapitals = function() {
		var countries ="https://restcountries.eu/rest/v1/all/";
    	$http({
    		url : countries,
    		method : "GET"
    	}).success(function(data, status, headers, config){
    		var capitals = [];
    		angular.forEach(data, function(value, key){
    			if(value.capital !== "") {
					capitals.push(value.capital.replace(/\n/g, ''));
				}
			});
			$scope.capitals.valid = true;
			$scope.capitals.response = capitals;
    	}).error(function(data, status, headers, config){
	    	$scope.capitals.valid = false;
	    });
    };

    $scope.getYahooUrl = function(city) {
    	$scope.message = city;
		var query = 
			"select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")";
		var url = 
			"https://query.yahooapis.com/v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q=";
	    return url + query.replace("%s", city);
    };

	$scope.update = function(city) {
    	$scope.message = city;
    	asyncCallWeather($scope.getYahooUrl(city));
    };

    $scope.start = function() {
    	$scope.update("Porto");
    	asyncCallCapitals();
    };

    $scope.start();

}]);