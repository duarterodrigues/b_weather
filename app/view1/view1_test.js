'use strict';

describe('myApp.view1 module', function() {

  beforeEach(module('myApp.view1'));

  describe('view1 controller', function(){

    it('should ....', inject(function($controller, $rootScope) {
      //spec body
      var $scope = $rootScope.$new();
      var view1Ctrl = $controller('View1Ctrl', {$scope: $scope});
      
      expect(view1Ctrl).toBeDefined();

  	}));


  	it(' yahoo url should be well formed', inject(function($controller, $rootScope) {
      //setup must be in before..
      var $scope = $rootScope.$new();
      var view1Ctrl = $controller('View1Ctrl', {$scope: $scope});
      //arrange
      var portoUrl = "https://query.yahooapis.com/v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"Porto\")";
      //act
      var url = $scope.getYahooUrl("Porto");
      //assert
      expect(url).toEqual(portoUrl);
  	}));

    it('should demonstrate using when (200 status)', inject(function($httpBackend, $http) {
  
  		var $scope={};

		/* Code Under Test */
		$http.get('http://localhost/foo')
		.success(function(data, status, headers, config) {
		  $scope.valid = true;
		  $scope.response = data;
		})
		.error(function(data, status, headers, config) {
		  $scope.valid = false;
		});
		/* End */

		$httpBackend.when('GET', 'http://localhost/foo').respond(200, { foo: 'bar' });

		$httpBackend.flush();

		expect($scope.valid).toBe(true);
  		expect($scope.response).toEqual({ foo: 'bar' });
	
	}));


  });
});