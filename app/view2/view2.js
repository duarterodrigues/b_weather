'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope',function($scope) {
	$scope.dev = {
		name : "Júlio Rodrigues",
		email : "julioduarterodrigues@gmail.com",
		phone : "(00351) 939 179 107",
		resume: "In my previous professional experiences I learnt to be autonomous and to work with a team, I also developed my sense of responsibility when I worked in the food industry, mainly because I was responsible, not only for my work, but also for the work of the employees that were on my team. When I worked in the Research sector, I learnt the importance of creating and following the procedures meticulously and I developed my communications skills by transferring that knowledge to others through training sessions. More recently, as I worked for the Commerce industry, I consolidated some of the qualities I mentioned earlier, and I also improved my linguistic skills and my ability to understand foreign cultures, due to the International dimension of the company I worked for. I have a degree in Computer Science from the University of Porto and am currently working on my Master’s Degree at the same University.My college years taught me a lot and helped me appreciate my work, and my profession.",
		env:"DISTRIB_ID=Ubuntu DISTRIB_RELEASE=14.04 DISTRIB_CODENAME=trusty DISTRIB_DESCRIPTION=\"Ubuntu 14.04.3 LTS\" NAME=\"Ubuntu\" VERSION=\"14.04.3 LTS, Trusty Tahr\" ID=ubuntu ID_LIKE=debian PRETTY_NAME=\"Ubuntu 14.04.3 LTS\" VERSION_ID=\"14.04\" HOME_URL=\"http://www.ubuntu.com/\" SUPPORT_URL=\"http://help.ubuntu.com/\" BUG_REPORT_URL=\"http://bugs.launchpad.net/ubuntu/\"",
		tech:"I chose angularJs to challenge myself, as it is my first time using this framework. I hope this simple and basic code suites your expectations.",
		repo:"https://bitbucket.org/duarterodrigues/b_weather"
	};
}]);